/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 *
 * @author Junyang
 * 
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property {
    public Property(int id, String address, int numberOfBedrooms, int size, double price) {
		super();
		this.id = id;
		this.address = address;
		this.numberOfBedrooms = numberOfBedrooms;
		this.size = size;
		this.price = price;
	}
	@Override
	public String toString() {
		return "Property [id=" + id + ", address=" + address + ", numberOfBedrooms=" + numberOfBedrooms + ", size="
				+ size + ", price=" + price + "]";
	}
	private int id;
    private String address;
    private int numberOfBedrooms;
    private int size;
    private double price;


        public Property() {
            this.id = 0;
            this.address = "unknown";
            this.numberOfBedrooms = 0;
            this.size = 0;
            this.price = 0;
        }
        
        public Property(Property property) {
            this.id = property.id;
            this.address = property.address;
            this.numberOfBedrooms = property.numberOfBedrooms;
            this.size = property.size;
            this.price = property.price;
        }

    public int getID()
    {
        return id;
    }
    
    public void setID(int newID)
    {
        id = newID;
    }
    
    public String getAddress()
    {
        return address;
    }
    
    public void setAddress(String newAddress)
    {
    	address = newAddress;
    }
    
    public int getNumberOfBedrooms()
    {
        return numberOfBedrooms;
    }
    
    public void setNumberOfBedrooms(int newNumberOfBedrooms)
    {
    	numberOfBedrooms = newNumberOfBedrooms;
    }   
    
    public int getSize()
    {
        return size;
    }
    
    public void setSize(int newSize)
    {
    	size = newSize;
    }
    
    public double getPrice()
    {
        return price;
    }
    
    public void setPrice(double newPrice)
    {
    	price = newPrice;
    }
    
}
